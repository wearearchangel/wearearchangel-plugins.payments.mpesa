# MPESA Client
Convenient client library to manage access to MPESA STK API

## Get started
install `composer require wearearchangel/mpesa`

## Concepts
Each call to API via the client will return an array in the following format:

    array(
        'code' => //server response code e.g 2XX or 4XX (NOT the same as mpesa error codes)
        'error_message' => //The error message incase of 4xx or 5xx error
        'data' => //decoded server response body in array format incase of successful request
    )


### Client

Access to the MPESA API is managed using the `Payments\MPESAClient`

#### Configuration

Initialize the `MPESAClient` as shown below
    
    use Payments\MPESAClient;
    
    $config = array(
        MPESAClient::CONFIG_API_KEY => 'api key here',
        MPESAClient::CONFIG_MPESA_SHORT_CODE => 'short code here',
        MPESAClient::CONFIG_MERCHANT_NAME => 'acturial',
        MPESAClient::CONFIG_SOURCE_APP => 'test',
        MPESAClient::CONFIG_STAGE => 'testing',
    );
    
    $mpesaClient = new MPESAClient($config);
   
    
#### Making a payment request

Make a payment request to a customer.


    $merchantRequestId = 56; // unique value for each request e.g transaction id
    $timestamp = $mpesaClient->getMPESATimestamp(); //Time in the format: yyyyMMDDHHmmss
    $amount = 50;
    $phoneNumber = "254713167623"; //use this same phone number for testing
    $callBackURL = "callback URL here";
    $transactionDesc = "Customer PayBill OnlineTransaction";
    
    $res = $mpesaClient->makePaymentRequest($merchantRequestId, $timestamp, $amount, $phoneNumber, $callBackURL,
        $transactionDesc); 
        
    var_dump($res);
    
#### MPESA Payment request Callback 

This is the request made by MPESA API to the specified `callBackURL`.

The format of the request body:

    {
        "stkCallback":{
            "MerchantRequestID":"CCCD68DE-4BB1",
            "ResultCode":"0",
            "ResultDesc":"The service request has been accepted successfully.",
            "CallbackMetadata":[
                {
                    "Item":{
                    "Name":"Amount",
                    "Value":"60"
                }
                },
                {
                    "Item":{
                    "Name":"MpesaReceiptNumber",
                    "Value":"KIF510PGLR"
                }
                },
                {
                    "Item":{
                    "Name":"Balance",
                    "Value":"{Amount={BasicAmount=604.45, MinimumAmount=60t445, Curre
                    ncyCode=KES}}"
                }
                },
                {
                "Item":{
                    "Name":"TransactionDate",
                    "Value":"20160915200515"
                    }
                },
                {
                    "Item":{
                    "Name":"PhoneNumber",
                    "Value":"0708835256"
                }
                }
            ]
        }
    }
    
    
#### Making a payment query

Make a payment request query to get more info about a payment request.
[Not Fully Implemented]


    $businessShortCode = 174379;
    $checkoutRequestId = 'checkoutRequestId'; //Returned from MPESA API after making payment request
    
    $res = $mpesaClient->makePaymentQuery($checkoutRequestId, $timestamp);
    var_dump($res);   
    

#### MPESA Result Codes

| Response  | Response Description |
|---------- | ---------------------|
|0          | Success. Request accepted for processing |
|1          | An exception occurred during processing|
|2          |Merchant does not exist|
|3          |Wrong credentials|
|4          |Merchant not allowed to carry out transaction type|
|CBS0001    | No ICCID found|
|23         |Request message structure is invalid|
|0          |The service request has been accepted successfully. The MPESA payment was successful|
|1          |The balance is insufficient for the transaction.|
|2          |Declined due to limit rule: less than the minimum transaction amount|
|3          |Declined due to limit rule: greater than the maximum transaction amount.|
|4          |Declined due to limit rule: would exceed the daily transfer limit.|
|5          |Declined due to limit rule: would be below the minimum balance.|
|8          |Declined due to limit rule: would exceed the maximum balance.|
|11         |The DebitParty is in an invalid state.|
|17         |System internal error.|
|26         |System busy. The service request is rejected.|
|1031       |Request cancelled by user|
|2001       |The initiator information is invalid.|
|2006       |Declined due to account rule: The account status does not allow this transaction.|    
       