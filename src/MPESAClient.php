<?php

namespace Payments;

class MPESAClient extends PaymentClient
{
    //Config params
    const CONFIG_API_KEY = "api_key";
    const CONFIG_MPESA_SHORT_CODE = "mpesa_short_code";
    const CONFIG_MERCHANT_NAME = "merchant_name";
    const CONFIG_SOURCE_APP = "source_app";
    const CONFIG_STAGE = "stage";

    //urls
    const URL_STAGE_TESTING = "http://196.201.214.172:9097/v1/stktransaction";
    const URL_STAGE_PRODUCTION = "https://196.201.214.155:9098/v1/stktransaction";

    /**
     * MPESAClient constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /**
     * Make payment request
     * @param $merchantRequestId
     * @param $timestamp
     * @param $amount
     * @param $phoneNumber
     * @param $callbackUrl
     * @param $transactionDesc
     * @return array
     */
    public function makePaymentRequest($merchantRequestId, $timestamp, $amount, $phoneNumber, $callbackUrl, $transactionDesc)
    {

        $merchantId = $this->config[self::CONFIG_MPESA_SHORT_CODE];
        $passKey = $this->config[self::CONFIG_API_KEY];
        $merchantName = $this->config[self::CONFIG_MERCHANT_NAME];
        $sourceApp = $this->config[self::CONFIG_SOURCE_APP];

        $password = base64_encode("$merchantId$passKey$timestamp");

        //body
        $requestJSONString = [
            "ns1:ProcessCheckout" => [
                "ProcessCheckoutRequest" => [
                    "checkoutHeader" => [
                        "BusinessShortCode" => $merchantId,
                        "Password" => $password,
                        "Timestamp" => $timestamp
                    ],
                    "checkoutTransaction" => [
                        "SourceApp" => $sourceApp,
                        "TransactionType" => "CustomerPayBillOnline",
                        "MerchantRequestID" => $merchantRequestId,
                        "Amount" => $amount,
                        "PartyA" => $phoneNumber,
                        "PartyB" => $merchantId,
                        "PhoneNumber" => $phoneNumber,
                        "CallBackURL" => $callbackUrl,
                        "ns1:Parameter" => [
                            "ReferenceItem" => [
                                [
                                    "Key" => "AccountReference",
                                    "Value" => $merchantId
                                ],
                                [
                                    "Key" => "MerchantName",
                                    "Value" => $merchantName
                                ]
                            ]
                        ],
                        "TransactionDesc" => $transactionDesc
                    ]
                ]
            ]];


        $body = array(
            "RequestJSOnString" => json_encode($requestJSONString)
        );

        return $this->makeRequest($this->getMPESAURL(), 'post', $body);
    }

    /**
     * Query the status of a payment
     * @param $checkoutRequestId
     * @param $timestamp
     * @return array
     */
    public function makePaymentQuery($checkoutRequestId, $timestamp)
    {

        $merchantId = $this->config[self::CONFIG_MPESA_SHORT_CODE];
        $passKey = $this->config[self::CONFIG_API_KEY];

        $password = base64_encode($merchantId . $passKey . $timestamp);

        //body
        $requestJSONString = [
            "ns1:QueryCheckout" => [
                "QueryCheckoutRequest" => [
                    "checkoutHeader" => [
                        "BusinessShortCode" => $merchantId,
                        "Password" => $password,
                        "Timestamp" => $timestamp
                    ],
                    "queryTransaction" => [
                        "CheckoutRequestID" => $checkoutRequestId
                    ]
                ]
            ]
        ];


        $body = array(
            "RequestJSOnString" => json_encode($requestJSONString)
        );

        return $this->makeRequest($this->getMPESAURL(), 'post', $body);
    }

    /**
     * Get the specified time in MPESA timestamp format
     * @param $unixTimeStamp int uses default time if not specified
     * @return string
     */
    public function getMPESATimestamp($unixTimeStamp = 0)
    {
        if ($unixTimeStamp <= 0) {
            $unixTimeStamp = time();
        }
        return date("Ymdhis", $unixTimeStamp);
    }

    private function getMPESAURL()
    {
        $stage = "testing";

        if (isset($this->config[self::CONFIG_STAGE]))
            $stage = $this->config[self::CONFIG_SOURCE_APP];

        switch ($stage) {
            case 'production':
                return self::URL_STAGE_PRODUCTION;
                break;
            case 'testing':
            default:
                return self::URL_STAGE_TESTING;
        }
    }
}