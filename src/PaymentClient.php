<?php

namespace Payments;

//Base class to be extended by all payment clients

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class PaymentClient
{
    //Response constants
    const RES_CODE = "code";
    const RES_ERROR_MESSAGE = "error_message";
    const RES_DATA = "data";

    /**
     * Store all configuration options for client
     * @var array
     */
    protected $config = array();

    /**
     * PaymentClient constructor.
     * @param $config array configuration options for this client
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Helper method to make requests
     * @param $url
     * @param string $method
     * @param null $body
     * @param null $headers
     * @return array response
     */
    protected function makeRequest($url, $method = 'post',  $body = null, $headers = null)
    {

        $response = $this->getDefaultResponse();

        try {
            $client = new Client(array("verify" => false));

            //Setup request
            $options = array();
            if($body != null) $options['form_params'] = $body;
            if($headers != null) $options['headers'] = $headers;

            $res = null;

            //Make request
            switch ($method){
                case 'post':
                    $res = $client->post($url, $options);
                    break;
                case 'get':
                    $res = $client->get($url, $options);
                    break;
            }

            //process response
            if($res != null){
                $response[self::RES_CODE] = $res->getStatusCode();
                $response[self::RES_DATA] = json_decode($res->getBody(), true);
            }
        } catch (RequestException $e) {
            var_dump($e->getRequest()->getBody()->getContents());
            $response[self::RES_CODE] = $e->getCode();
            $response[self::RES_ERROR_MESSAGE] = $e->getMessage();
        }

        return $response;
    }

    /**
     * Helper function to generate default response
     * @return array
     */
    protected function getDefaultResponse()
    {
        return array(
            PaymentClient::RES_CODE => null,
            PaymentClient::RES_ERROR_MESSAGE => null,
            PaymentClient::RES_DATA => null
        );
    }
}